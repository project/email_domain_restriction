<?php

/**
 * @file
 * File: Email domain restriction module.
 *
 * Restricts or allow input of a particular email domain.
 */

/**
 * Implements hook_permission().
 */
function email_domain_restriction_permission() {
  return array(
    'administer email domain restriction' => array(
      'title' => t('Administer email domain restriction'),
      'description' => t('Allows or deny email domains.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function email_domain_restriction_menu() {
  $items['admin/config/people/email_domain_restriction'] = array(
    'title' => 'Email domain restriction',
    'description' => 'Allows or deny email domains',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('email_domain_restriction_admin_form'),
    'access arguments' => array('administer email domain restriction'),
    'file' => 'email_domain_restriction.admin.inc',
    'module' => 'email_domain_restriction',
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_form_form_id_form_alter().
 */
function email_domain_restriction_form_user_register_form_alter(&$form, &$form_state, $form_id) {
  $form['#validate'][] = 'email_domain_restriction_email_domain_validate';
}

/**
 * Implements hook_form_form_id_form_alter().
 */
function email_domain_restriction_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
  $apply_existing_email = variable_get('email_domain_restriction_apply_current_fields', 0);
  if ($apply_existing_email == 1) {
    $form['#validate'][] = 'email_domain_restriction_email_domain_validate';
  }
}

/**
 * Custom validation function.
 */
function email_domain_restriction_email_domain_validate(&$form, &$form_state) {
  $validation = email_domain_restriction_validate_email(array($form_state['values']['mail']));
  if ($validation == FALSE) {
    form_set_error('mail', email_domain_restriction_error_message());
  }
}

/**
 * Implements hook_user_login().
 */
function email_domain_restriction_user_login(&$edit, $account) {
  $apply_existing_email = variable_get('email_domain_restriction_apply_current_fields', 0);
  if ($apply_existing_email == 1) {
    $validation = email_domain_restriction_validate_email(array($account->mail));
    if ($validation == FALSE) {
      $message = t('Your email is not valid. Please change it as soon as possible');
      drupal_set_message($message, 'warning');
    }
  }
}

/**
 * Check if the email domain is in the list.
 *
 * It accept an array of email as parameter. It can be used in other form.
 *
 * @param array $emails
 *   Email to check.
 *
 * @return bool
 *   FALSE if not passed, TRUE if passed.
 */
function email_domain_restriction_validate_email(array $emails) {
  if (!empty($emails)) {
    $emails_array = array();
    foreach ($emails as $email) {
      $email = explode('@', $email);
      $emails_array[] = strtolower($email[1]);
    }
  }
  else {
    return FALSE;
  }
  $domains_list = email_domain_restriction_email_domain_list();
  $behaviour = variable_get('email_domain_restriction_behaviour', 0);
  if ($behaviour == 0) {
    foreach ($emails_array as $email) {
      if (in_array($email, $domains_list)) {
        return FALSE;
      }
    }
  }
  else {
    foreach ($emails_array as $email) {
      if (!in_array($email, $domains_list)) {
        return FALSE;
      }
    }
  }
  return TRUE;
}

/**
 * Retrive email domain list.
 *
 * @return array $email_list
 * - List of email domains.
 */
function email_domain_restriction_email_domain_list() {
  $email = variable_get('email_domain_restriction_list', '');
  $email = $email != '' ? explode("\r\n", $email) : array();
  return $email;
}

/**
 * Retrive error message.
 *
 * @return string $message
 * - Error message.
 */
function email_domain_restriction_error_message() {
  $message = variable_get('email_domain_restriction_message', t('Email domain is not valid'));
  $show_list = FALSE;
  $show_list = variable_get('email_domain_restriction_show_list_email');

  if ($show_list == TRUE) {
    $email_list = implode('<br />', email_domain_restriction_email_domain_list());
    $behaviour = variable_get('email_domain_restriction_behaviour', 0);
    if ($behaviour == 0) {
      $email_list_message = t('Email domains denied:') . '<br />';
    }
    else {
      $email_list_message = t('Email domains allowed:') . '<br />';
    }
    $email_list_message .= $email_list;
    $message .= '<br />' . $email_list_message;
  }

  return $message;
}